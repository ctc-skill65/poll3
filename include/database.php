<?php

class DB {
    public static $conn;

    public static function connect()
    {
        self::$conn = new mysqli(
            conf('db_host'),
            conf('db_user'),
            conf('db_password'),
            conf('db_name')
        );

        if (self::$conn->connect_error) {
            exit(self::$conn->connect_error);
        }

        self::$conn->set_charset(conf('db_charset'));
    }

    public static function query($sql)
    {
        return self::$conn->query($sql);
    }

    public static function result($sql) 
    {
        $result = self::query($sql);

        $data = [];
        while ($row = $result->fetch_assoc()) {
            $data[] = $row;
        }

        return $data;
    }

    public static function row($sql) 
    {
        $result = self::query($sql);
        return $result->fetch_assoc();
    }
}

DB::connect();
