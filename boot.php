<?php

date_default_timezone_set('Asia/Bangkok');
define('DATE_SQL', 'Y-m-d H:m:s');

define('ROOT', __DIR__);
define('INC', __DIR__ . '/include');

session_start();
$flash = null;
if (isset($_SESSION['flash'])) {
    $flash = $_SESSION['flash'];
    unset($_SESSION['flash']);
}

$config = require ROOT . '/config.php';

require_once INC . '/common.php';
require_once INC . '/database.php';
require_once INC . '/auth.php';
