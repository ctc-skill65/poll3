<?php

return [
    'app_name' => 'ระบบแบบสํารวจออนไลน์ (Poll)',
    'site_url' => 'http://skill65.local/poll3',

    'db_host' => 'localhost',
    'db_user' => 'root',
    'db_password' => 'root',
    'db_name' => 'skill65_poll3',
    'db_charset' => 'utf8mb4'
];
