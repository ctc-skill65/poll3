<?php
require_once __DIR__ . '/../../boot.php';
checkAuth('user');

$poll_id = get('poll');
$page_path = "/user/polls/edit.php?poll={$poll_id}";

$action = get('action');
$id = get('id');

switch ($action) {
    case 'delete':
        $sql = "DELETE FROM `questions` WHERE `q_id`='{$id}'";
        break;
}

if (isset($sql)) {
    DB::query($sql);
    redirect($page_path);
}

if (isset($_POST['poll_name'])) {
    $qr = DB::query("UPDATE `polls` SET 
    `poll_name`='{$_POST['poll_name']}',
    `poll_type_id`='{$_POST['poll_type_id']}' 
    WHERE `poll_id`='{$poll_id}'");
    
    if ($qr) {
        setAlert('success', "แก้ไขแบบสำรวจสำเร็จเรียบร้อย");
    } else {
        setAlert('error', "เกิดข้อผิดพลาด ไม่สามารถแก้ไขแบบสำรวจได้");
    }
    redirect($page_path);
}

if (isset($_POST['q_name'])) {
    $qr = DB::query("INSERT INTO `questions`(
    `poll_id`, 
    `q_name`) 
    VALUES (
    '{$poll_id}',
    '{$_POST['q_name']}')");
    
    if ($qr) {
        setAlert('success', "เพิ่มแบบสำรวจสำเร็จเรียบร้อย");
        $q_id = DB::$conn->insert_id;
        redirect("/user/polls/edit-question.php?poll={$poll_id}&q={$q_id}");
    } else {
        setAlert('error', "เกิดข้อผิดพลาด ไม่สามารถเพิ่มแบบสำรวจได้");
    }
    redirect($page_path);
}

$data = DB::row("SELECT * FROM `polls` WHERE `poll_id`='{$poll_id}'");
$poll_types = DB::result("SELECT * FROM `poll_types`");
$items = DB::result("SELECT * FROM `questions` WHERE `poll_id`='{$poll_id}'");
ob_start();
?>
<a href="<?= url("/user/polls/list.php") ?>">
    <button>< กลับ</button>
</a>
<?= showAlert() ?>
<h3>แก้ไขแบบสำรวจ</h3>
<form method="post">
    <label for="poll_name">ชื่อแบบสำรวจ</label>
    <input type="text" name="poll_name" id="poll_name" value="<?= $data['poll_name'] ?>" required>
    <br>
    <label for="poll_type_id">ประเภทแบบสำรวจ</label>
    <select name="poll_type_id" id="poll_type_id">
        <option value="" selected disabled>---- เลือก ----</option>
        <?php foreach ($poll_types as $item) : ?>
            <option value="<?= $item['poll_type_id'] ?>" <?= $item['poll_type_id'] === $data['poll_type_id'] ? 'selected' : '' ?>><?= $item['poll_type_name'] ?></option>
        <?php endforeach; ?>
    </select>
    <br>
    <button type="submit">บันทึก</button>
</form>

<h3>เพื่อคำถาม</h3>
<form method="post">
    <label for="q_name">คำถาม</label>
    <input type="text" name="q_name" id="q_name" required>
    <br>
    <button type="submit">บันทึก</button>
</form>

<h3>รายการคำถาม</h3>
<table>
    <thead>
        <th>รหัส</th>
        <th>คำถาม</th>
        <th>จัดการคำถาม</th>
    </thead>
    <tbody>
        <?php foreach ($items as $item) : ?>
            <tr>
                <td><?= $item['q_id'] ?></td>
                <td><?= $item['q_name'] ?></td>
                <td>
                    <a href="<?= url("/user/polls/edit-question.php?poll={$item['poll_id']}&q={$item['q_id']}") ?>">
                        แก้ไข
                    </a>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="<?= url($page_path) ?>&action=delete&id=<?= $item['q_id'] ?>" <?= clickConfirm("คุณต้องการลบคำถาม {$item['q_name']} หรือไม่") ?>>
                        ลบ
                    </a>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php
$layout_page = ob_get_clean();
$page_name = 'แก้ไขแบบสำรวจ';
require ROOT . '/user/layout.php';
