<?php
require_once __DIR__ . '/../../boot.php';
checkAuth('user');

$page_path = "/user/profile/edit-pass.php";

if ($_POST) {
    if (md5(post('password')) !== $user['password']) {
        setAlert('error', "รหัสผ่านปัจจุบันไม่ถูกต้อง");
        redirect($page_path);
    }

    $hash = md5(post('password_new'));
    $qr = DB::query("UPDATE `users` SET 
    `password`='{$hash}' 
    WHERE `user_id`='{$user_id}'");
    
    if ($qr) {
        setAlert('success', "แก้ไขรหัสผ่านสำเร็จเรียบร้อย");
    } else {
        setAlert('error', "เกิดข้อผิดพลาด ไม่สามารถแก้ไขรหัสผ่านได้");
    }
    redirect($page_path);
}

ob_start();
?>
<?= showAlert() ?>
<form method="post">
    <label for="password">รหัสผ่านปัจจุบัน</label>
    <input type="password" name="password" id="password" required>
    <br>

    <label for="password_new">รหัสผ่านใหม่</label>
    <input type="password" name="password_new" id="password_new" required>
    <br>

    <button type="submit">บันทึก</button>
</form>
<?php
$layout_page = ob_get_clean();
$page_name = 'แก้ไขรหัสผ่าน';
require ROOT . '/user/layout.php';
